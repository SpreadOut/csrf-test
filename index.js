var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();

app.use(cors({
  origin: '*',
  credentials: true,
}));


app.use(cookieParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app
  .get('/login', (req, res) => {
    console.log(req.cookies);
    res.send(`
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>CSRF Test</title>
      </head>
      <body>
        <form action="/login" method="post">
          Email:<br>
          <input type="text" name="id">
          <br>Password:<br>
          <input type="text" name="password">
          <br><br>
          <input type="submit" value="Submit">
        </form>
      </body>
      </html>
    `);
  })
  .post('/login', (req, res) => {
    console.log(req.body);
    res.cookie('testSessionID', 'testtest', { maxAge: 900000, httpOnly: true });
    res.status(200).send(
      `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>CSRF Test</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      </head>
      <body>
        Login success
        <button id="test">test</button>
        <div id="tt"></div>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#test").click(function () {
              $.post("http://localhost/deleteImportantData", function (data) {
                $("#tt").html(data);
              });
            });
          });
        </script>
      </body>
      </html>
      `
    );
  });

app.post('/deleteImportantData', (req, res) => {
  console.log('Cookies: ', req.cookies);
  if (req.cookies.testSessionID === 'testtest') {
    res.status(200).send(
      `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>CSRF Test</title>
      </head>
      <body>
        DELETE VERY IMPORTANT DATA!!!!!!!
      </body>
      </html>
      `
    );
  } else {
    res.redirect('/login');
  }
});

app.listen(80, function () {
  console.log('Example app listening on port 80!');
});
