# CSRF Test

This is a simple demo of CSRF attack.

# How to Run this Demo

### Step 1
```shell
# cd to project dir
cd csrf-test

# install dependencies
npm i

# start server
sudo node index.js
```

- navigate to `http://localhost/login` and type any email and password to login, it will store a test session cookie to your browser.

### Step 2

- open index.html in your browser, and click play. If this website shows `DELETE IMPORTANT DATA` it means this csrf attack is success.


